package hu.bme.aut.android.trainingmetronome.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import hu.bme.aut.android.trainingmetronome.activity.TrackActivity
import hu.bme.aut.android.trainingmetronome.data.Track
import hu.bme.aut.android.trainingmetronome.databinding.CardTrackBinding

class TrackAdapter(private val context: Context) :
    ListAdapter<Track, TrackAdapter.TrackViewHolder>(TrackCallback) {

    private val trackList: MutableList<Track> = mutableListOf()
    private val trackIdMap: MutableMap<Track, String> = mutableMapOf()
    private var lastPosition = -1

    fun addTrack(newTrack: Track, id: String) {
        trackIdMap[newTrack] = id
        trackList += newTrack
        submitList(trackList)
        notifyItemInserted(trackList.lastIndex)
        Log.d(TAG, "addTrack:called")
    }

    fun modifyTrack(modifiedTrack: Track, id: String) {
        val index = trackList.indexOfFirst { track: Track ->
            trackIdMap[track].equals(id)
        }
        trackIdMap.remove(trackList[index])
        trackList[index] = modifiedTrack
        trackIdMap[trackList[index]] = id
        submitList(trackList)
        notifyItemChanged(index)
        Log.d(TAG, "modifyTrack:called")
    }

    fun removeTrack(id: String) {
        val index = trackList.indexOfFirst { track: Track ->
            trackIdMap[track].equals(id)
        }
        trackIdMap.remove(trackList[index])
        trackList.removeAt(index)
        submitList(trackList)
        notifyItemRemoved(index)
        Log.d(TAG, "removeTrack:called")
    }

    interface TrackAdapterCallback {
        fun editTrackClicked(trackId: String, trackToEdit: Track)
        fun deleteTrackClicked(trackId: String)
    }

    inner class TrackViewHolder(binding: CardTrackBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val mBinding = binding
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        TrackViewHolder(
            CardTrackBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: TrackViewHolder, position: Int) {
        val tmpTrack = trackList[position]
        holder.mBinding.tvName.text = tmpTrack.name
        setAnimation(holder.itemView, position)
        holder.mBinding.root.setOnClickListener {
            context.startActivity(TrackActivity.createIntent(context, tmpTrack))
        }
        holder.mBinding.btnDelete.setOnClickListener {
            (context as TrackAdapterCallback).deleteTrackClicked(trackIdMap[tmpTrack]!!)
        }
        holder.mBinding.btnEdit.setOnClickListener {
            (context as TrackAdapterCallback).editTrackClicked(trackIdMap[tmpTrack]!!, tmpTrack)
        }
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    companion object {
        private const val TAG = "TrackAdapter"

        object TrackCallback : DiffUtil.ItemCallback<Track>() {
            override fun areItemsTheSame(oldTrack: Track, newTrack: Track): Boolean {
                return oldTrack == newTrack
            }

            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(oldTrack: Track, newTrack: Track): Boolean {
                return oldTrack == newTrack
            }
        }
    }

}
