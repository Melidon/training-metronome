package hu.bme.aut.android.trainingmetronome.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.result.ActivityResultCallback
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.AuthUI.IdpConfig
import com.firebase.ui.auth.AuthUI.IdpConfig.EmailBuilder
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.FirebaseAuthUIActivityResultContract
import com.firebase.ui.auth.IdpResponse
import com.firebase.ui.auth.data.model.FirebaseAuthUIAuthenticationResult
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import hu.bme.aut.android.trainingmetronome.R
import hu.bme.aut.android.trainingmetronome.databinding.ActivityAuthUiBinding

class AuthUiActivity : AppCompatActivity(),
    ActivityResultCallback<FirebaseAuthUIAuthenticationResult> {

    companion object {
        private const val TAG = "AuthUiActivity"
        fun createIntent(context: Context): Intent {
            return Intent(context, AuthUiActivity::class.java)
        }
    }

    private lateinit var mBinding: ActivityAuthUiBinding
    private val signIn = registerForActivityResult(FirebaseAuthUIActivityResultContract(), this)

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = ActivityAuthUiBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mBinding.signIn.setOnClickListener { signIn() }
    }

    private fun signIn() {
        signIn.launch(getSignInIntent())
    }

    private fun getSignInIntent(): Intent {
        return AuthUI.getInstance().createSignInIntentBuilder()
            .setTheme(R.style.Theme_TrainingMetronome)
            .setLogo(R.drawable.ic_launcher_foreground)
            .setAvailableProviders(providers)
            .build()
    }

    override fun onResume() {
        super.onResume()
        val auth = FirebaseAuth.getInstance()
        if (auth.currentUser != null && intent.extras == null) {
            startMainActivity(null)
            finish()
        }
    }

    private fun handleSignInResponse(resultCode: Int, response: IdpResponse?) {
        // Successfully signed in
        if (resultCode == RESULT_OK) {
            startMainActivity(response)
            finish()
        } else {
            // Sign in failed
            if (response == null) {
                // User pressed back button
                showSnackbar(R.string.sign_in_cancelled)
                return
            }
            if (response.error!!.errorCode == ErrorCodes.NO_NETWORK) {
                showSnackbar(R.string.no_internet_connection)
                return
            }
            if (response.error!!.errorCode == ErrorCodes.ERROR_USER_DISABLED) {
                showSnackbar(R.string.account_disabled)
                return
            }
            showSnackbar(R.string.unknown_error)
            Log.e(TAG, "Sign-in error: ", response.error)
        }
    }

    private fun startMainActivity(response: IdpResponse?) {
        startActivity(MainActivity.createIntent(this, response))
    }

    private val providers: List<IdpConfig>
        get() {
            return arrayListOf(
                EmailBuilder().build(),
                // // TODO: Majd ha már minden készen van
                // GoogleBuilder().build(),
            )
        }

    private fun showSnackbar(@StringRes errorMessageRes: Int) {
        Snackbar.make(mBinding.root, errorMessageRes, Snackbar.LENGTH_LONG).show()
    }

    override fun onActivityResult(result: FirebaseAuthUIAuthenticationResult) {
        // Successfully signed in
        val response = result.idpResponse
        handleSignInResponse(result.resultCode, response)
    }

}
