package hu.bme.aut.android.trainingmetronome.data

import java.io.Serializable

class Track(
    var name: String? = null,
    var numberOfRepetitions: Long? = null,
    var keepUpTime: Long? = null,
    var goDownTime: Long? = null,
    var keepDownTime: Long? = null,
    var goUpTime: Long? = null,
) : Serializable {
    fun toHashMap(): HashMap<String, Any?> {
        return hashMapOf(
            "name" to name,
            "numberOfRepetitions" to numberOfRepetitions,
            "keepUpTime" to keepUpTime,
            "goDownTime" to goDownTime,
            "keepDownTime" to keepDownTime,
            "goUpTime" to goUpTime,
        )
    }
}
