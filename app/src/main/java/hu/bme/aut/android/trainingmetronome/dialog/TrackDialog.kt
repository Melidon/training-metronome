package hu.bme.aut.android.trainingmetronome.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import hu.bme.aut.android.trainingmetronome.R
import hu.bme.aut.android.trainingmetronome.data.Track
import hu.bme.aut.android.trainingmetronome.databinding.DialogTrackBinding
import kotlin.properties.Delegates


class TrackDialog : DialogFragment() {

    interface TrackHandler {
        fun createTrack(trackToCreate: Track)
        fun updateTrack(trackId: String, trackToUpdate: Track)
    }

    companion object {
        const val TRACK_ID = "TRACK_ID"
        const val TRACK_TO_EDIT = "TRACK_TO_EDIT"
    }

    private lateinit var mBinding: DialogTrackBinding

    private var isEditDialog by Delegates.notNull<Boolean>()
    private lateinit var trackHandler: TrackHandler

    private var name: String
        get() = mBinding.etName.text.toString()
        set(value) {
            mBinding.etName.setText(value)
        }
    private var numberOfRepetitions: Long
        get() = mBinding.etNumberOfRepetitions.text.toString().toLong()
        set(value) {
            mBinding.etNumberOfRepetitions.setText(value.toString())
        }
    private var keepUpTime: Long
        get() = (mBinding.etKeepUpTime.text.toString().toDouble() * 1000).toLong()
        set(value) {
            mBinding.etKeepUpTime.setText((value.toDouble() / 1000).toString())
        }
    private var goDownTime: Long
        get() = (mBinding.etGoDownTime.text.toString().toDouble() * 1000).toLong()
        set(value) {
            mBinding.etGoDownTime.setText((value.toDouble() / 1000).toString())
        }
    private var keepDownTime: Long
        get() = (mBinding.etKeepDownTime.text.toString().toDouble() * 1000).toLong()
        set(value) {
            mBinding.etKeepDownTime.setText((value.toDouble() / 1000).toString())
        }
    private var goUpTime: Long
        get() = (mBinding.etGoUpTime.text.toString().toDouble() * 1000).toLong()
        set(value) {
            mBinding.etGoUpTime.setText((value.toDouble() / 1000).toString())
        }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is TrackHandler) {
            trackHandler = context
        } else {
            throw RuntimeException("The Activity does not implement the TrackHandler interface")
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        mBinding = DialogTrackBinding.inflate(LayoutInflater.from(context))
        isEditDialog = (arguments != null && requireArguments().containsKey(TRACK_TO_EDIT))
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder
                .setTitle(
                    when (isEditDialog) {
                        true -> R.string.edit_existing_track
                        false -> R.string.create_new_track
                    }
                )
                .setView(mBinding.root)
                .setPositiveButton(
                    when (isEditDialog) {
                        true -> R.string.edit
                        false -> R.string.create
                    }, null
                )
                .setNegativeButton(R.string.cancel) { _, _ -> dialog?.cancel() }
            setDefaultValues()
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onResume() {
        super.onResume()

        val dialog = dialog as AlertDialog
        val positiveButton = dialog.getButton(Dialog.BUTTON_POSITIVE)

        positiveButton.setOnClickListener {
            if (validate()) {
                when (isEditDialog) {
                    true -> handleTrackEdit()
                    false -> handleTrackCreate()
                }
                dialog.dismiss()
            }
        }
    }

    private fun setDefaultValues() {
        if (isEditDialog) {
            val trackToEdit = requireArguments().getSerializable(TRACK_TO_EDIT) as Track
            name = trackToEdit.name!!
            numberOfRepetitions = trackToEdit.numberOfRepetitions!!
            keepUpTime = trackToEdit.keepUpTime!!
            goDownTime = trackToEdit.goDownTime!!
            keepDownTime = trackToEdit.keepDownTime!!
            goUpTime = trackToEdit.goUpTime!!
        } else {
            name = "Your new track"
        }
    }

    private fun validate(): Boolean {

        if (mBinding.etName.text.isEmpty()) {
            mBinding.etName.error = getString(R.string.it_can_not_be_empty)
            return false
        }

        if (mBinding.etNumberOfRepetitions.text.isEmpty()) {
            mBinding.etNumberOfRepetitions.error =
                getString(R.string.it_can_not_be_empty)
            return false
        }
        if (numberOfRepetitions <= 0) {
            mBinding.etNumberOfRepetitions.error = getString(R.string.it_must_be_at_least_one)
            return false
        }

        if (mBinding.etKeepUpTime.text.isEmpty()) {
            mBinding.etKeepUpTime.error = getString(R.string.it_can_not_be_empty)
            return false
        }

        if (mBinding.etGoDownTime.text.isEmpty()) {
            mBinding.etGoDownTime.error = getString(R.string.it_can_not_be_empty)
            return false
        }
        if (goDownTime < 400) {
            mBinding.etGoDownTime.error = getString(R.string.the_minimum_is, 0.4)
            return false
        }

        if (mBinding.etKeepDownTime.text.isEmpty()) {
            mBinding.etKeepDownTime.error = getString(R.string.it_can_not_be_empty)
            return false
        }

        if (mBinding.etGoUpTime.text.isEmpty()) {
            mBinding.etGoUpTime.error = getString(R.string.it_can_not_be_empty)
            return false
        }
        if (goDownTime < 400) {
            mBinding.etGoUpTime.error = getString(R.string.the_minimum_is, 0.4)
            return false
        }

        return true
    }

    private fun handleTrackCreate() {
        trackHandler.createTrack(
            Track(
                name,
                numberOfRepetitions,
                keepUpTime,
                goDownTime,
                keepDownTime,
                goUpTime,
            )
        )
    }

    private fun handleTrackEdit() {
        val trackToEdit = requireArguments().getSerializable(TRACK_TO_EDIT) as Track
        trackToEdit.name = name
        trackToEdit.numberOfRepetitions = numberOfRepetitions
        trackToEdit.keepUpTime = keepUpTime
        trackToEdit.goDownTime = goDownTime
        trackToEdit.keepDownTime = keepDownTime
        trackToEdit.goUpTime = goUpTime
        val trackId = requireArguments().getString(TRACK_ID)!!
        trackHandler.updateTrack(trackId, trackToEdit)
    }
}
