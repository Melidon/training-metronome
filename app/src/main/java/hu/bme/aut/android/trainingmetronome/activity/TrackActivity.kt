package hu.bme.aut.android.trainingmetronome.activity

import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import hu.bme.aut.android.trainingmetronome.R
import hu.bme.aut.android.trainingmetronome.data.Track
import hu.bme.aut.android.trainingmetronome.databinding.ActivityTrackBinding
import java.util.concurrent.atomic.AtomicBoolean

class TrackActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "TrackActivity"
        private const val TRACK = "TRACK"

        fun createIntent(context: Context, track: Track): Intent {
            return Intent(context, TrackActivity::class.java).putExtra(TRACK, track)
        }
    }

    private lateinit var mBinding: ActivityTrackBinding
    private lateinit var track: Track
    private var isRunning: AtomicBoolean = AtomicBoolean(false)
    private var loopThread: Thread? = null
    private lateinit var keepUpSound: MediaPlayer
    private lateinit var goDownSound: MediaPlayer
    private lateinit var keepDownSound: MediaPlayer
    private lateinit var goUpSound: MediaPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = ActivityTrackBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        track = (intent.getSerializableExtra(TRACK) as Track?)!!

        keepUpSound = MediaPlayer.create(this, R.raw.keep_up_sound)
        goDownSound = MediaPlayer.create(this, R.raw.go_down_sound)
        keepDownSound = MediaPlayer.create(this, R.raw.keep_down_sound)
        goUpSound = MediaPlayer.create(this, R.raw.go_up_sound)

        mBinding.imageView.setOnClickListener {
            if (isRunning.get()) {
                stopLoop()
            } else {
                startLoop()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        stopLoop()
    }

    private fun startLoop() {
        loopThread = Thread { loop() }
        loopThread!!.start()
        isRunning.set(true)
    }

    private fun stopLoop() {
        loopThread?.interrupt()
        setToDefault()
    }

    private fun setToDefault() {
        runOnUiThread {
            mBinding.imageView.setImageResource(R.drawable.ic_play_circle_outline)
        }
        isRunning.set(false)
    }

    private fun loop() {
        try {
            for (i in 1..track.numberOfRepetitions!!) {
                keepUp()
                goDown()
                keepDown()
                goUp()
            }
            setToDefault()
        } catch (e: InterruptedException) {

        }
    }

    private fun keepUp() {
        if (track.keepUpTime!! > 0) {
            runOnUiThread {
                mBinding.imageView.setImageResource(R.drawable.ic_pending)
            }
            keepUpSound.start()
            Thread.sleep(track.keepUpTime!!)
            keepUpSound.pause()
        }
    }

    private fun goDown() {
        runOnUiThread {
            mBinding.imageView.setImageResource(R.drawable.ic_arrow_circle_down)
        }
        goDownSound.start()
        Thread.sleep(track.goDownTime!!)
        goDownSound.pause()
    }

    private fun keepDown() {
        if (track.keepDownTime!! > 0) {
            runOnUiThread {
                mBinding.imageView.setImageResource(R.drawable.ic_pending)
            }
            keepDownSound.start()
            Thread.sleep(track.keepDownTime!!)
            keepDownSound.pause()
        }
    }

    private fun goUp() {
        runOnUiThread {
            mBinding.imageView.setImageResource(R.drawable.ic_arrow_circle_up)
        }
        goUpSound.start()
        Thread.sleep(track.goUpTime!!)
        goUpSound.pause()
    }

}
