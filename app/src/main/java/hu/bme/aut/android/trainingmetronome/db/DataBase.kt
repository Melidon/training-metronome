package hu.bme.aut.android.trainingmetronome.db

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import hu.bme.aut.android.trainingmetronome.data.Track

class DataBase {

    companion object {
        private const val TAG = "DataBase"

        private const val USERS = "users"
        private const val TRACKS = "tracks"

        private var instance = DataBase()
        fun getInstance(): DataBase {
            return instance
        }
    }

    private val db = Firebase.firestore
    private val auth = FirebaseAuth.getInstance()

    private val user: FirebaseUser?
        get() = auth.currentUser

    private val tracks: CollectionReference?
        get() {
            user ?: return null
            return db.collection(USERS).document(user!!.uid).collection(TRACKS)
        }

    fun createTrack(trackToCreate: Track) {
        tracks
            ?.add(trackToCreate.toHashMap())
            ?.addOnSuccessListener { documentReference ->
                Log.d(TAG, "Track added with ID: ${documentReference.id}")
            }
            ?.addOnFailureListener { error ->
                Log.w(TAG, "Error adding track", error)
            }
    }

    fun addTracksListener(listener: EventListener<QuerySnapshot?>) {
        tracks
            ?.addSnapshotListener(listener)
    }

    fun updateTrack(trackId: String, trackToUpdate: Track) {
        tracks
            ?.document(trackId)
            ?.update(trackToUpdate.toHashMap())
            ?.addOnSuccessListener {
                Log.d(TAG, "Track updated with ID: $trackId")
            }
            ?.addOnFailureListener { error ->
                Log.w(TAG, "Error updated track", error)
            }
    }

    fun deleteTrack(trackId: String) {
        tracks
            ?.document(trackId)
            ?.delete()
            ?.addOnSuccessListener {
                Log.d(TAG, "Track deleted with ID: $trackId")
            }
            ?.addOnFailureListener { error ->
                Log.w(TAG, "Error deleting track", error)
            }
    }

}
