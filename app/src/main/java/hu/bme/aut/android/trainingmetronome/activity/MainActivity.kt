package hu.bme.aut.android.trainingmetronome.activity

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.firebase.ui.auth.util.ExtraConstants
import com.google.android.gms.tasks.Task
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.ktx.toObject
import hu.bme.aut.android.trainingmetronome.R
import hu.bme.aut.android.trainingmetronome.adapter.TrackAdapter
import hu.bme.aut.android.trainingmetronome.data.Track
import hu.bme.aut.android.trainingmetronome.databinding.ActivityMainBinding
import hu.bme.aut.android.trainingmetronome.db.DataBase
import hu.bme.aut.android.trainingmetronome.dialog.TrackDialog

class MainActivity : AppCompatActivity(), TrackAdapter.TrackAdapterCallback,
    TrackDialog.TrackHandler {

    companion object {
        private const val TAG = "MainActivity"
        fun createIntent(context: Context, response: IdpResponse?): Intent {
            return Intent().setClass(context, MainActivity::class.java)
                .putExtra(ExtraConstants.IDP_RESPONSE, response)
        }
    }

    private lateinit var mBinding: ActivityMainBinding
    private lateinit var trackAdapter: TrackAdapter

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser == null) {
            startActivity(AuthUiActivity.createIntent(this))
            finish()
            return
        }

        mBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        setSupportActionBar(mBinding.toolbar)

        trackAdapter = TrackAdapter(this)
        mBinding.rvTracks.layoutManager = LinearLayoutManager(this).apply {
            reverseLayout = true
            stackFromEnd = true
        }
        mBinding.rvTracks.adapter = trackAdapter

        mBinding.floatingActionButton.setOnClickListener {
            createTrackClicked()
        }

        val response: IdpResponse? = intent.getParcelableExtra(ExtraConstants.IDP_RESPONSE)
        handleResponse(response)

        initTrackListener();
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.main_activity_toolbar, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.sign_out -> {
                signOut()
                true
            }
            R.id.delete_account -> {
                deleteAccountClicked()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun deleteAccountClicked() {
        MaterialAlertDialogBuilder(this)
            .setMessage(R.string.are_you_sure_you_want_to_delete_this_account)
            .setPositiveButton(R.string.yes_delete_it) { _: DialogInterface?, _: Int -> deleteAccount() }
            .setNegativeButton(R.string.no, null)
            .show()
    }

    private fun deleteAccount() {
        AuthUI.getInstance()
            .delete(this)
            .addOnCompleteListener(
                this
            ) { task: Task<Void?> ->
                if (task.isSuccessful) {
                    startActivity(AuthUiActivity.createIntent(this@MainActivity))
                    finish()
                } else {
                    showSnackbar(R.string.delete_account_failed)
                }
            }
    }

    private fun signOut() {
        AuthUI.getInstance()
            .signOut(this)
            .addOnCompleteListener { task: Task<Void?> ->
                if (task.isSuccessful) {
                    startActivity(AuthUiActivity.createIntent(this@MainActivity))
                    finish()
                } else {
                    Log.w(
                        TAG,
                        "signOut:failure",
                        task.exception
                    )
                    showSnackbar(R.string.sign_out_failed)
                }
            }
    }

    private fun handleResponse(response: IdpResponse?) {
        response ?: return;
        if (response.isNewUser) {
            // TODO: Majd ha kedvem lesz hozzá, meg túléltem ezt a félévet. Úgysincsen benne a specifikációban.
        }
    }

    private fun initTrackListener() {
        DataBase.getInstance().addTracksListener() { snapshots, error ->
            if (error != null) {
                Log.w(TAG, "Error at snapshot listener", error)
                return@addTracksListener
            }
            for (dc in snapshots!!.documentChanges) {
                when (dc.type) {
                    DocumentChange.Type.ADDED -> {
                        trackAdapter.addTrack(
                            dc.document.toObject(),
                            dc.document.id
                        )
                    }
                    DocumentChange.Type.MODIFIED -> {
                        trackAdapter.modifyTrack(
                            dc.document.toObject(),
                            dc.document.id
                        )
                    }
                    DocumentChange.Type.REMOVED -> {
                        trackAdapter.removeTrack(dc.document.id)
                    }
                }
            }
        }
    }

    private fun createTrackClicked() {
        TrackDialog().show(supportFragmentManager, null)
    }

    override fun createTrack(trackToCreate: Track) {
        DataBase.getInstance().createTrack(trackToCreate)
    }

    override fun editTrackClicked(trackId: String, trackToEdit: Track) {
        val editDialog = TrackDialog()
        val bundle = Bundle()
        bundle.putString(TrackDialog.TRACK_ID, trackId)
        bundle.putSerializable(TrackDialog.TRACK_TO_EDIT, trackToEdit)
        editDialog.arguments = bundle
        editDialog.show(supportFragmentManager, null)
    }

    override fun updateTrack(trackId: String, trackToUpdate: Track) {
        DataBase.getInstance().updateTrack(trackId, trackToUpdate)
    }

    override fun deleteTrackClicked(trackId: String) {
        AlertDialog.Builder(this)
            .setMessage(getString(R.string.are_you_sure_you_want_to_delete_this_track))
            .setPositiveButton(getString(R.string.yes_delete_it)) { _, _ -> deleteTrack(trackId) }
            .setNegativeButton(getString(R.string.no), null)
            .show()
    }

    private fun deleteTrack(trackId: String) {
        DataBase.getInstance().deleteTrack(trackId)
    }

    private fun showSnackbar(@StringRes errorMessageRes: Int) {
        Snackbar.make(mBinding.root, errorMessageRes, Snackbar.LENGTH_LONG).show()
    }

}
